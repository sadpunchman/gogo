import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Library } from './scenes/Library';
import { Stories } from './scenes/Stories';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Library />} />
        <Route path="/stories" element={<Stories />} />
      </Routes>
    </Router>
  );
}

export default App;
