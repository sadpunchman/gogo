import './styles.scss'

type ToggleButtonProps = {
    leftText: string,
    rightText: string,
    selected: boolean,
    toggleSelected: (event: any) => void,
    containerClassName?: string,
    leftTextClassName?: string,
    rightTextClassName?: string,
}

const ToggleButton = (props: ToggleButtonProps) => {
    const { leftText, rightText, selected, toggleSelected, containerClassName, leftTextClassName, rightTextClassName } = props;
    
    const containerClasses = containerClassName ? `${containerClassName} ` : ''
    const leftTextClasses = leftTextClassName ? `${leftTextClassName} ` : ''
    const rightTextClasses = rightTextClassName ? `${rightTextClassName} ` : ''
    
    return (
        <div className={`${containerClasses}toggle-container custom-border-solid-dark-gray-1 mb-2`}
                onClick={toggleSelected}>

                    <div className={`${leftTextClasses}dialog-button fw-bold d-flex dialog-button-on ${selected 
                            ? 'toggle-button-on' : 'toggle-button-disabled'}`}>
                            {leftText}
                    </div>

                    <div className={`${rightTextClasses}dialog-button dialog-button-off fw-bold d-flex ${selected 
                            ? 'toggle-button-disabled' : 'toggle-button-off'}`}>
                            {rightText}
                    </div>

        </div>
    )
}

export default ToggleButton;
