import React from 'react';
import style from './style.module.scss';

type Props = { children: React.ReactNode };

const GeneralButton = ({ children }: Props) => (
  <button type="button" className={style.generalButton}>
    <p className={style.generalButtonTitle}>{children}</p>
  </button>
);

export default GeneralButton;
