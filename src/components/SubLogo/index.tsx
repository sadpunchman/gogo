import React from 'react';
import subLogo from './sub-logo.png';
import style from './style.module.scss';

const SubLogo = () => (
  <img className={style.subLogo} alt="sub-logo" src={subLogo} />
);

export default SubLogo;
