/* eslint-disable react/require-default-props */
import React from 'react';
import classNames from 'classnames';
import style from './style.module.scss';

type Props = {
  children: string;
  active?: boolean;
};

export const StrokeText = ({ children, active = false }: Props) => (
  <span
    data-before={children}
    className={classNames(style.strokeText, {
      [style.strokeTextActive]: active,
    })}
  >
    {children}
  </span>
);
