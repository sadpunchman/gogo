import React from 'react';
import classNames from 'classnames';
import { useResolvedPath, useMatch, Link } from 'react-router-dom';
import Logo from '../Logo';
import Search from './Search';
import Burger from './Burger';
import { StrokeText } from '../Typography';
import style from './style.module.scss';

const NAV_ITEMS = [
  {
    label: 'Library',
    to: '/',
  },
  {
    label: 'Favorites',
    to: '/favorites',
  },
  {
    label: 'Account',
    to: '/account',
  },
];

const NavListItem = ({
  navItem,
}: {
  navItem: {
    label: string;
    to: string;
  };
}) => {
  const resolved = useResolvedPath(navItem.to);
  const match = useMatch({ path: resolved.pathname, end: true });

  return (
    <li className={style.navListItem}>
      <Link to={navItem.to} className={style.navListItemLink}>
        <StrokeText active={!!match}>{navItem.label}</StrokeText>
      </Link>
    </li>
  );
};

const Header = ({
  high = false,
  sticky = false,
}: {
  high?: boolean;
  sticky?: boolean;
}) => (
  <header
    className={classNames(style.header, {
      [style.headerHigh]: high,
      [style.headerSticky]: sticky,
    })}
  >
    <div className={style.headerContentWrap}>
      <Logo />
      <nav className={style.nav}>
        <ul className={style.navList}>
          {NAV_ITEMS.map((navItem) => (
            <NavListItem navItem={navItem} />
          ))}
        </ul>
        <Search />
        <Burger />
      </nav>
    </div>
  </header>
);

export default Header;
