import React from 'react';
import logo from './logo.svg';
import style from './style.module.scss';

const Logo = () => <img className={style.logo} alt="logo" src={logo} />;
export default Logo;
