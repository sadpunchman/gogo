import React from 'react';
import CarouselWithTitle from '../../CarouselWithTitle';
import style from './style.module.scss';

export type TabsContent = {
  title: string;
  data: number[];
};

type Props = {
  tabsContent: TabsContent[];
  active: boolean;
  titleColor: string;
  arrowColor: string;
};

const TabContent = ({ tabsContent, active, titleColor, arrowColor }: Props) =>
  active ? (
    <div className={style.tabContent}>
      {tabsContent.map((tabsContentItem) => (
        <div key={tabsContentItem.title} className={style.tabContentItem}>
          <CarouselWithTitle
            {...tabsContentItem}
            titleColor={titleColor}
            arrowColor={arrowColor}
          />
        </div>
      ))}
    </div>
  ) : null;

export default TabContent;
