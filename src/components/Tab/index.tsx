import React, { useState } from 'react';
import Carousel from 'react-multi-carousel';
import Container from '../Container';
import NavigationButton from '../NavigationButton';
import TabContent, { TabsContent } from './TabContent';
import style from './style.module.scss';

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 2000, min: 3000 },
    items: 6,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 7,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 7,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 5,
  },
};

type NavigationButtons = {
  icon: React.ReactElement;
  label: string;
  activeColor: string;
  tabTitleColor: string;
  tabContentColor: string;
  arrowColor: string;
};

type Props = {
  navigationButtons: NavigationButtons[];
  tabsContent: TabsContent[][];
};

const Tab = ({ navigationButtons, tabsContent }: Props) => {
  const firstNavButton = navigationButtons[0];
  const [activeIndex, setActiveIndex] = useState(0);
  const [title, setTitle] = useState(firstNavButton.label);
  const [tabTitleColor, setTabTitleColor] = useState(
    firstNavButton.tabTitleColor
  );
  const [tabContentColor, setTabContentColor] = useState(
    firstNavButton.tabContentColor
  );
  const [arrowColor, setArrowColor] = useState(firstNavButton.arrowColor);

  return (
    <Container>
      <Carousel
        partialVisible
        responsive={responsive}
        arrows={false}
        containerClass={style.carouselContainerClass}
        itemClass={style.carouselItemPadding}
      >
        {navigationButtons.map((navigationButton, index) => (
          <NavigationButton
            {...navigationButton}
            active={index === activeIndex}
            onClick={() => {
              setActiveIndex(index);
              setTitle(navigationButton.label);
              setTabTitleColor(navigationButton.tabTitleColor);
              setTabContentColor(navigationButton.tabContentColor);
              setArrowColor(navigationButton.arrowColor);
            }}
          />
        ))}
      </Carousel>
      <h3 className={style.tabContentTitle} style={{ color: tabTitleColor }}>
        {title}
      </h3>
      {tabsContent.map((tabContent, index) => (
        <TabContent
          arrowColor={arrowColor}
          tabsContent={tabContent}
          active={index === activeIndex}
          titleColor={tabContentColor}
        />
      ))}
    </Container>
  );
};

export default Tab;
