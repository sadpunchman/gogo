// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React from 'react';
import className from 'classnames';
import { ReactComponent as ChevronLeft } from '../../../assets/icons/chevron-left.svg';
import { ReactComponent as ChevronRight } from '../../../assets/icons/chevron-right.svg';
import style from './style.module.scss';

type Props = {
  left?: boolean;
  color?: string;
  onClick?: () => unknown;
};

const Arrow = ({ left, color, onClick = () => undefined }: Props) => (
  <button
    type="button"
    className={className(style.arrow, { [style.left]: left })}
    style={{ '--color': color }}
    onClick={onClick}
  >
    {left ? <ChevronLeft /> : <ChevronRight />}
  </button>
);

export default Arrow;
