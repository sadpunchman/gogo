/* eslint-disable jsx-a11y/control-has-associated-label */
import React from 'react';
import classNames from 'classnames';
import style from './style.module.scss';

type Props = {
  active?: boolean;
  onClick?: () => unknown;
};

const CarouselDot = ({ active = false, onClick = () => undefined }: Props) => (
  <button
    type="button"
    className={classNames(style.dot, { [style.active]: active })}
    onClick={onClick}
  />
);

export default CarouselDot;
