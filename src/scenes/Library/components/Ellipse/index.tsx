import React from 'react';
import style from './style.module.scss';

const Ellipse = () => (
  <div className={style.ellipseRoot}>
    <div className={style.ellipse} />
  </div>
);

export default Ellipse;
