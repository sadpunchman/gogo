import React from 'react';
import Carousel from 'react-multi-carousel';
import CarouselDot from '../CarouselDot';
import Container from '../../../../components/Container';
import style from './style.module.scss';

const BOXES = [1, 2, 3];

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 2000, min: 3000 },
    items: 3,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 767 },
    items: 3,
  },
  mobile: {
    breakpoint: { max: 767, min: 0 },
    items: 1,
  },
};

const Box = () => (
  <div className={style.box}>
    <div className={style.boxGray} />
  </div>
);

const FirstSection = () => (
  <div className={style.root}>
    <Container>
      <div className={style.firstSection}>
        <Carousel
          showDots
          renderDotsOutside
          responsive={responsive}
          arrows={false}
          slidesToSlide={0}
          containerClass={style.firstSectionCarouselContainer}
          itemClass={style.firstSectionCarouselItemPadding}
          dotListClass={style.firstSectionCarouselDotListClass}
          customDot={<CarouselDot />}
        >
          {BOXES.map((item) => (
            <Box key={item} />
          ))}
        </Carousel>
      </div>
    </Container>
  </div>
);

export default FirstSection;
