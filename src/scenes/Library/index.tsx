import React, { useEffect, useRef, useState } from 'react';

import Header from '../../components/Header';
import Footer from '../../components/Footer';

import Tab from '../../components/Tab';
import Ellipse from './components/Ellipse';
import FirstSection from './components/FirstSection';

import { ReactComponent as Stories } from '../../icons/stories.svg';
import { ReactComponent as Games } from '../../icons/games.svg';
import { ReactComponent as Songs } from '../../icons/songs.svg';
import { ReactComponent as GoGoCam } from '../../icons/goGoCam.svg';
import { ReactComponent as Printables } from '../../icons/printables.svg';
import { ReactComponent as ChoiceBoard } from '../../icons/choiceBoard.svg';
import style from './style.module.scss';

const data = [
  [
    {
      title: 'New Releases ',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Popular Picks',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Continue the Adventure',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Simple & Sweet',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
  ],
  [
    {
      title: 'New Releases',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'GoGo Finds',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Matching Games',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Coming Soon',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
  ],
  [
    {
      title: 'New Releases',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'GoGo Originals',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Popular Children’s Songs',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Coming Soon',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
  ],
  [
    {
      title: 'New Releases ',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Popular Picks',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Continue the Adventure',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Simple & Sweet',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
  ],
  [
    {
      title: 'New Releases',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'GoGo Finds',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Matching Games',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Coming Soon',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
  ],
  [
    {
      title: 'New Releases',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'GoGo Originals',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Popular Children’s Songs',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
    {
      title: 'Coming Soon',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    },
  ],
];

const NAVIGATION_BUTTONS = [
  {
    icon: <Stories />,
    label: 'Stories',
    activeColor: '#33ACFE',
    tabTitleColor: '#33ACFE',
    tabContentColor: '#2670A3',
    arrowColor: '#33ACFE',
  },
  {
    icon: <Games />,
    label: 'Games',
    activeColor: '#33D291',
    tabTitleColor: '#33D291',
    tabContentColor: '#217350',
    arrowColor: '#33D291',
  },
  {
    icon: <Songs />,
    label: 'Songs',
    activeColor: '#EFD81C',
    tabTitleColor: '#EFD81C',
    tabContentColor: '#70641B',
    arrowColor: '#EFD81C',
  },
  {
    icon: <Printables />,
    label: 'Printables',
    activeColor: '#EB1268',
    tabTitleColor: '#EB1268',
    tabContentColor: '#971144',
    arrowColor: '#EB1268',
  },
  {
    icon: <GoGoCam />,
    label: 'GoGo Cam',
    activeColor: '#F59C07',
    tabTitleColor: '#F59C07',
    tabContentColor: '#F59C07',
    arrowColor: '#F59C07',
  },
  {
    icon: <ChoiceBoard />,
    label: 'Choice Board',
    activeColor: '#740EE5',
    tabTitleColor: '#740EE5',
    tabContentColor: '#740EE5',
    arrowColor: '#740EE5',
  },
];

export const Library = () => {
  const mainRef = useRef<any>(null);
  const [isScroledToContent, setIsScroledToContent] = useState(false);
  const minimalScrollContentValue = 90;

  const onScroll = () => {
    const clientRectTop = mainRef?.current?.getBoundingClientRect()?.top;
    if (clientRectTop < minimalScrollContentValue) {
      setIsScroledToContent(true);
      return;
    }
    setIsScroledToContent(false);
  };

  useEffect(() => {
    window.addEventListener('scroll', onScroll);
    return () => window.removeEventListener('scroll', onScroll);
  }, []);

  return (
    <main className={style.library} onScroll={onScroll}>
      <Header high={!isScroledToContent} sticky={isScroledToContent} />
      <Ellipse />
      <div ref={mainRef} className={style.libraryContent}>
        <FirstSection />
        <Tab navigationButtons={NAVIGATION_BUTTONS} tabsContent={data} />
      </div>
      <Footer />
    </main>
  );
};
