import React from 'react';
import StrokeText from '../StrokeText';
import style from './style.module.scss';

const ReadyToGoButton = () => (
  <button type="button" className={style.readyToGoButton}>
    <div className={style.buttonTitle}>
      <StrokeText color="#217350">Ready to GoGo</StrokeText>
    </div>
  </button>
);

export default ReadyToGoButton;
