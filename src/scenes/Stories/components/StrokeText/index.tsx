/* eslint-disable react/require-default-props */
import React from 'react';
import style from './style.module.scss';

type Props = {
  children: string;
  color?: string;
};

const StrokeText = ({ children, color }: Props) => (
  <span
    data-before={children}
    className={style.strokeText}
    style={
      {
        '--color-stroke': color,
      } as { [key: string]: string }
    }
  >
    {children}
  </span>
);

export default StrokeText;
