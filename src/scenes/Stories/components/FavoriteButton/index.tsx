import React from 'react';
import { ReactComponent as FavoriteIcon } from '../../src/favorite.svg';
import StrokeText from '../StrokeText';
import style from './style.module.scss';

const FavoriteButton = () => (
  <button type="button" className={style.favoriteButton}>
    <div className={style.favoriteIconWrap}>
      <div className={style.favoriteIconCover}>
        <FavoriteIcon className={style.favoriteIcon} />
      </div>
      <div className={style.buttonTitle}>
        <StrokeText color="#2670A3">Add Favorite</StrokeText>
      </div>
    </div>
  </button>
);

export default FavoriteButton;
