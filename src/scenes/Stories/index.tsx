import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../../components/Header';
import ArrowButton from '../../components/ArrowButton';
import ReadyToGoButton from './components/ReadyToGoButton';
import FavoriteButton from './components/FavoriteButton';
import GeneralButton from '../../components/GeneralButton';
import CarouselWithTitle from '../../components/CarouselWithTitle';
import { ReactComponent as VideoIcon } from './src/play.svg';
import { ReactComponent as NotesIcon } from './src/notes.svg';
import CampDragonBG from './src/CampDragonBG.png';
import style from './style.module.scss';

const Description = () => (
  <div className={style.description}>
    <div className={style.descriptionActions}>
      <ReadyToGoButton />
      <FavoriteButton />
    </div>
    <div className={style.descriprionTextWrap}>
      <p className={style.descriprionText}>
        The perfect kindling to spark up some great conversation and target all
        of your kids` speech and language goals! This activity will take you
        into the woods to join our dragon friend for a fun-filled camping
        adventure. You and your kids will have so much fun as you help the
        dragon unpack his bag, set up camp, and roast some yummy food to feed
        his appetite. We all know that a camping trip is not complete without
        s’mores, so don`t you worry— there will be plenty of marshmallows for us
        all! 🏕️ 🔥
      </p>
    </div>
  </div>
);

const BUTTONS = [
  {
    label: 'Speech and Language Target Lists',
    to: '/targetList',
  },

  {
    label: 'Review Companion Book',
    to: '/review',
  },

  {
    label: 'View Lesson Plan',
    to: '/lessonPlan',
  },
];

const Lesson = () => (
  <div className={style.lesson}>
    <div className={style.lessonVideo}>
      <div className={style.lessonVideoIconWrap}>
        <VideoIcon className={style.lessonVideoIcon} />
      </div>
      <div className={style.lessonVideoTitleWrap}>
        <div className={style.lessonVideoTitle}>Story Walkthrough</div>
        <NotesIcon className={style.notesIcon} />
      </div>
    </div>
    <div className={style.lessonActionList}>
      {BUTTONS.map((lessonButton) => (
        <div className={style.lessonButton}>
          <Link to={lessonButton.to} className={style.link}>
            <GeneralButton>{lessonButton.label}</GeneralButton>
          </Link>
        </div>
      ))}
    </div>
  </div>
);

export const Stories = () => (
  <main>
    <Header />
    <div className={style.stories}>
      <ArrowButton className={style.arrowButton} />
      <div className={style.content}>
        <div className={style.contentDescriptionWrap}>
          <div className={style.contentDescription}>
            <h1 className={style.storieTitle}>Camp Dragon</h1>
            <div className={style.campDragonBG}>
              <img src={CampDragonBG} alt="Dragon" />
            </div>
            <Description />
          </div>
          <div className={style.campDragonBGTablet}>
            <img src={CampDragonBG} alt="Dragon" />
          </div>
        </div>
        <Lesson />
        <div className={style.storiesSlider}>
          <CarouselWithTitle
            title="Related Resources"
            data={[1, 2, 3, 4, 5, 6, 7, 9]}
            titleColor="#2670A3"
            arrowColor="#33ACFE"
          />
        </div>
      </div>
    </div>
  </main>
);
