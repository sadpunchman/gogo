# GoGo Speech

This is a frontend for GoGo Speech
## How to run it

```shell
git clone <REPO_NAME>
npm install
npm run dev
```

## Architecture

Frontend is build with [React](https://reactjs.org/) + [Vite](https://vitejs.dev/) + [TypeScript](https://www.typescriptlang.org/)  

Basic packages:
- [SASS](https://sass-lang.com/)

#### Scenes File Tree 

```shell
/src
  /components  #shared components
    /Button
    /Notifications
    /RadioButton

  /api #shared api config

  /types #shared types (TypeScript specific)

  /hooks #shared hooks

  /models #shared models (TypeScript specific)
  
  /stores #shared stores
  
  /services #shared services
    /constants.ts #shared constants
    /enums.ts #shared enums (TypeScript specific)
    /variables.scss
 
  /scenes #scenes (feature modules)
    /Home
      /HomeScene.tsx
      
      /components
        /ComponentItem
      
      /models
        /HomeModel.ts
      
      /services
        /enums.ts
        /constants.ts
        /HomeStore.ts
        /types.ts
        /hooks.ts
        
      /scenes #scenes for Home directory
        
  /main.tsx
```
